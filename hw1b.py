import os
from os import walk
import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy.linalg as linalg

from PIL import Image

import theano
import theano.tensor as T


'''
Implement the functions that were not implemented and complete the
parts of main according to the instructions in comments.
'''

def reconstructed_image(D,c,num_coeffs,X_mean,im_num):
    '''
    This function reconstructs an image given the number of
    coefficients for each image specified by num_coeffs
    '''
    
    '''
        Parameters
    ---------------
    c: np.ndarray
        a n x m matrix  representing the coefficients of all the image blocks.
        n represents the maximum dimension of the PCA space.
        m is (number of images x n_blocks**2)

    D: np.ndarray
        an N x n matrix representing the basis vectors of the PCA space
        N is the dimension of the original space (number of pixels in a block)

    im_num: Integer
        index of the image to visualize

    X_mean: np.ndarray
        a matrix representing the mean block.

    num_coeffs: Integer
        an integer that specifies the number of top components to be
        considered while reconstructing
    '''
    
    c_im = c[:num_coeffs,im_num]
    D_im = D[:,:num_coeffs]
    
    #TODO: Enter code below for reconstructing the image
    intde_img = np.dot(np.transpose(c_im),np.transpose(D_im)) 
    sz = int(np.sqrt(len(intde_img[0])))
    intde_img = [intde_img[i].reshape((sz,sz)) for i in xrange(len(intde_img))]
    intde_img = [intde_img[i] + X_mean for i in xrange(len(intde_img))]
    intde_img = np.array(intde_img)
    
    X_recon_img = []
    for ii in xrange(n_blocks):
        for jj in xrange(sz):
            row = []
            for kk in xrange(n_blocks):
                row.append(intde_img[n_blocks * ii + kk][jj])
            X_recon_img.append(np.array(row).flatten())
    X_recon_img = np.array(X_recon_img)
    X_recon_img = X_recon_img.astype(int)
    X_recon_img = np.transpose(X_recon_img)
        
    return X_recon_img

def plot_reconstructions(D,c,num_coeff_array,X_mean,im_num):
    '''
    Plots 9 reconstructions of a particular image using D as the basis matrix and coeffiecient
    vectors from c

    Parameters
    ------------------------
        num_coeff_array: Iterable
            an iterable with 9 elements representing the number_of coefficients
            to use for reconstruction for each of the 9 plots
        
        c: np.ndarray
            a l x m matrix  representing the coefficients of all blocks in a particular image
            l represents the dimension of the PCA space used for reconstruction
            m represents the number of blocks in an image

        D: np.ndarray
            an N x l matrix representing l basis vectors of the PCA space
            N is the dimension of the original space (number of pixels in a block)

        X_mean: basis vectors represent the divergence from the mean so this
            matrix should be added to all reconstructed blocks

        im_num: Integer
            index of the image to visualize
    '''
    f, axarr = plt.subplots(3,3)
    for i in range(3):
        for j in range(3):
            plt.axes(axarr[i,j])
            plt.imshow(reconstructed_image(D,c,num_coeff_array[i*3+j],X_mean,im_num))
            
    f.savefig('output/hw1b_{0}.png'.format(im_num))
    plt.close(f)
    
    
    
def plot_top_16(D, sz, imname):
    '''
    Plots the top 16 components from the basis matrix D.
    Each basis vector represents an image block of shape (sz, sz)

    Parameters
    -------------
    D: np.ndarray
        N x n matrix representing the basis vectors of the PCA space
        N is the dimension of the original space (number of pixels in a block)
        n represents the maximum dimension of the PCA space (assumed to be atleast 16)

    sz: Integer
        The height and width of each block

    imname: string
        name of file where image will be saved.
    '''
    #TODO: Obtain top 16 components of D and plot them
    top_16 = [D[:,i].reshape((sz,sz)) for i in xrange(16)]
    f,axarr = plt.subplots(4,4)
    for i in range(4):
        for j in range(4):
            plt.axes(axarr[i,j])
            plt.imshow(top_16([i*4+j], cmap='Greys_r')
                
    f.savefig(imname))
    plt.close(f)
    

def PCA(X, blockSize, maxIterations, learnRate, n_vectors):
    eigenVectors = []
    eigenValues = []
    diff0 = 0.1
    initial_v = np.random.uniform(0, 1, blockSize**2)
    initial_v = initial_v / linalg.norm(initial_v)
    current_vec = theano.shared(initial_v, name="current_vec")
    Xcurrent_vec = T.dot(X, current_vec)
    cost = -(T.dot(Xcurrent_vec.T, Xcurrent_vec) - np.sum(eigenValues[j]*T.dot(eigenVectors[j], current_vec)*T.dot(eigenVectors[j], current_vec) for j in xrange(len(eigenValues))))     
    gv = T.grad(cost, current_vec)
    y = current_vec - learnRate*gv
    update_expression = y / y.norm(2)        
    func_gd = theano.function([],
      current_vec,
      updates=[(current_vec, update_expression)]
    )
    for i in xrange(n_vectors):
        print 'i: ', i
        initial_v = np.random.uniform(0, 1, blockSize**2)
        initial_v = initial_v / linalg.norm(initial_v)
        current_vec.set_value(initial_v)                        
        iterations = 0
        diff = 100
        while iterations < maxIterations and diff > diff0:
            print 'iteraions: ', iterations
            prev_current_vec = current_vec.get_value()
            func_gd()
            diff = np.sum(abs(current_vec.get_value() - prev_current_vec))
            print 'diff: ', diff
            print 'cost: ', cost.eval()
            iterations = iterations + 1
        curr_eigenValue = T.dot(Xcurrent_vec.T, Xcurrent_vec).eval()
        eigenValues.append(curr_eigenValue)
        eigenVectors.append(current_vec.get_value())
    
    return eigenValues, eigenVectors

    
def main():
    '''
    Read here all images(grayscale) from Fei_256 folder and collapse 
    each image to get an numpy array Ims with size (no_images, height*width).
    Make sure the images are read after sorting the filenames
    '''
    #TODO: Write a code snippet that performs as indicated in the above comment
    Images = []
    nomages = 200
    for i in xrange(nomages):
        jpgImg = Image.open('/home/rahulrana/Documents/NeuralNetHW1/Dataset/Fei_256/' + 'image' + str(i) + '.jpg', 'r')
        pixels = jpgImg.load()
        col, row = jpgImg.size
        imageData = [[pixels[r,c] for c in xrange(col)] for r in xrange(row)]
        Images.append(imageData)
    
    Images = np.array(Images)
    Ims = Images.astype(np.float32)
    X_mn = np.mean(Ims, 0)
    X = Ims - np.repeat(X_mn.reshape(1, -1), Ims.shape[0], 0)

    '''
    Use theano to perform gradient descent to get top 16 PCA components of X
    Put them into a matrix D with decreasing order of eigenvalues

    If you are not using the provided AMI and get an error "Cannot construct a ufunc with more than 32 operands" :
    You need to perform a patch to theano from this pull(https://github.com/Theano/Theano/pull/3532)
    Alternatively you can downgrade numpy to 1.9.3, scipy to 0.15.1, matplotlib to 1.4.2
    '''
    
    #TODO: Write a code snippet that performs as indicated in the above comment
    eigenValues, eigenVectors = PCA(X, 256, 10, 0.001, 32)
    print eigenValues
    eigenValues = np.array(eigenValues)
    eigenVectors = np.array(eigenVectors)
    eigenVectors = eigenVectors.T
    idx = eigenValues.argsort()[::-1]   
    eigenValues = eigenValues[idx]
    D = eigenVectors[:,idx]
    c = np.dot(D.T, X.T)
        
    for i in range(0, 200, 10):
        plot_reconstructions(D=D, c=c, num_coeff_array=[1, 2, 4, 6, 8, 10, 12, 14, 16], X_mean=X_mean.reshape((256, 256)), im_num=i)

    plot_top_16(D, 256, 'output/hw1b_top16_256.png')


if __name__ == '__main__':
    main()
    
    
